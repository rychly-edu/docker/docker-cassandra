# Apache Cassandra Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-cassandra/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-cassandra/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-cassandra/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-cassandra/commits/master)

The image is based on [cassandra Docker repo](https://hub.docker.com/_/cassandra/).
The version of the base image (its Docker tag) can be restricted on build by the `FROM_TAG` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-cassandra:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-cassandra" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run by Docker-Compose

See [docker-compose.yml](docker-compose.yml).

The Docker image containers are scalable by on the the following commands:

~~~sh
docker-compose up --scale cassandra=2
docker-compose scale cassandra=2
~~~

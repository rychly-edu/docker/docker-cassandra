ARG FROM_TAG
FROM cassandra:${FROM_TAG:-latest}

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["cassandra", "-f"]

HEALTHCHECK CMD /healthcheck.sh
